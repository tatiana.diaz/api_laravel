<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Estudiantes;
use App\Models\Asociacion;

class EstudiantesController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $estudiantes = Estudiantes::get();

        return $estudiantes;
    }

    public function byId(Request $request){
        $estudiantes = Estudiantes::findOrFail($request->id);

        return $estudiantes;
    }

    public function create(Request $request){
        $estudiante = new Estudiantes;

        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->primer_apellido = $request->primer_nombre;
        $estudiante->identificacion = $request->identificacion;

        if($estudiante->save()){
            return "Estudiantes Guardado Correctamente";
        }else{
            return "Estudiantes No Guardado";
        }
        
    }

    public function edit(Request $request){
        $estudiantes = Estudiantes::findOrFail($request->id);

        return $estudiantes;
    }

    public function update(Request $request){

        $estudiante = Estudiantes::findOrFail($request->id);

        $estudiante->primer_nombre = $request->primer_nombre;
        $estudiante->primer_apellido = $request->primer_nombre;
        $estudiante->identificacion = $request->identificacion;

        if($estudiante->save()){
            return "Estudiantes Actualizado Correctamente";
        }else{
            return "Estudiantes No Actualizado";
        }
    }
    public function destroy(Request $request){
        $estudiante = Estudiantes::findOrFail($request->id);
        $estudiante->delete();
    }
    public function asociacion(Request $request){

        $asociacion = new Asociacion;
        $asociacion->id_estudiante = $request->id_estudiante;
        $asociacion->id_curso =$request->id_curso;
        if($asociacion
        ->save()){
            return "Asociacion Correctamente";
        }else{
            return "Asociacion Error";
        }
    }
}
