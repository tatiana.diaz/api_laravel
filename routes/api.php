<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EstudiantesController;
use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/estudiantes",'App\Http\Controllers\EstudiantesController@index');

Route::get("/estudiante/{id}",'App\Http\Controllers\EstudiantesController@byId');

Route::post("/crear_estudiante",'App\Http\Controllers\EstudiantesController@create');

Route::post("/editar_estudiante",'App\Http\Controllers\EstudiantesController@update');

Route::get("/eliminar/{id}",'App\Http\Controllers\EstudiantesController@destroy');

Route::post("/asociacion",'App\Http\Controllers\EstudiantesController@asociacion');

Route::get("/ApiRest",function(){
    $client = new Client([
        'base_uri'=>'https://randomuser.me/api/',
        'time_out'=>2.0
    ]);
    $response =  $client->request('GET');
    $response = $response->getBody()->getContents();
    $response = json_decode($response);
    print_r($response);die;
});
